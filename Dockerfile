FROM ruby:2.5-alpine

WORKDIR /app
COPY $CI_PROJECT_DIR /app/

RUN sed -i 's/dl-cdn.alpinelinux.org/mirrors.ustc.edu.cn/g' /etc/apk/repositories && \
    apk add --no-cache make git libc-dev gcc g++ libxml2 libxslt mysql-dev mysql-client nodejs tzdata yarn && \
    cp /usr/share/zoneinfo/Asia/Shanghai /etc/localtime && \
    gem sources --add https://gems.ruby-china.com/ --remove https://rubygems.org/ && \
    gem install bundler -v 1.16.3 --no-document && \
    bundle config mirror.https://rubygems.org https://gems.ruby-china.com && \
    bundle install --without development test && \
    rake assets:precompile RAILS_ENV=production && \
    apk del make gcc g++ tzdata yarn
